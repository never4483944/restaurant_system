<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('item', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger("user_id");
            $table->foreign('user_id')->references('id')->on('staff');
            $table->unsignedBigInteger("supplier_id");
            $table->foreign('supplier_id')->references('id')->on('supplier');
            $table->unsignedBigInteger("cagetogry_id");
            $table->foreign('cagetogry_id')->references('id')->on('item_category');
            $table->string("name", 100);
            $table->double("qauntity");
            $table->double("unit_price");
            $table->double("total_price");
            $table->timestamp("recieves_date");            
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('item');

    }
};
