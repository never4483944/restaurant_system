<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('staff', function (Blueprint $table) {
            $table->id();
            $table->string("username",255);
            $table->string("email",255);
            $table->string("password",255);
            $table->string("phone_number",255);
            $table->string("address",255);
            $table->unsignedBigInteger("role_id");
            $table->date("hire_date");
            $table->double("salary");
            $table->timestamp("updated_at");
            $table->smallInteger("status");

            $table->foreign('role_id')->references('role_id')->on('role'); 
         });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('role');
    }
};
